const SUIT_TYPES = {
    HEARTS: 'Hearts',
    DIAMONDS: 'Diamonds',
    CLUBS: 'Clubs',
    SPADES: 'Spades'
};
const RANK = {
    1: 'Ace',
    2: '2',
    3: '3',
    4: '4',
    5: '5',
    6: '6',
    7: '7',
    8: '8',
    9: '9',
    10: '10',
    11: 'Jack',
    12: 'Queen',
    13: 'King'
}

class Card {

    constructor(options) {
        this.suit = options.suit
        this.rank = options.rank
    }

    get isFaceCard() {
        return this.rank === 1 || this.rank > 10;
    }

    toString() {
        return `${RANK[this.rank]} of ${this.suit}`;
    }

    static compare(cardOne, cardTwo) {
        if(cardOne.rank === cardTwo.rank) {
            return 0;
        }
        if(cardOne.rank === 1) {
            return cardOne;
        } else if (cardTwo.rank === 1) {
            return cardTwo;
        }
        return cardOne.rank > cardTwo.rank ? cardOne : cardTwo;
    }
}

class Player {

    constructor(options) {
        this.name = options.name
        this.deck = options.deck
        this._wins = options.wins
        this._score = options.score
    }

    static play(playerOne, playerTwo) {
        startGame(playerOne.deck, playerOne, playerTwo);
    }

    get wins() {
        return this._wins;
    }

    set win(winCount){
        this._wins = winCount;
    }

    get score(){
        return this._score;
    }

    set newScore(newScore) {
        this._score = newScore;
    }

}

class Deck {

    constructor(options) {
        this.cards = options.cards
        this._count = options.count
    }

    shuffle(){
        for (let cardsCount = this.cards.length - 1; cardsCount > 0; cardsCount--) {
            const randomIndex = Math.floor(Math.random() * (cardsCount + 1));
            [this.cards[cardsCount], this.cards[randomIndex]] = [this.cards[randomIndex], this.cards[cardsCount]];
        }
    }

    draw(n) {
        const deletedCards = [];
        for(let cardCount = 0; cardCount < n; cardCount++) {
            deletedCards.push(this.cards.pop());
        }
        return deletedCards;
    }

    get count(){
        return this._count;
    }

    set newCount(newCount){
        this._count = newCount;
    }
}

function generateDeck() {
    const newDeck = [];
    for(let rankCount in RANK) {
        for(let suit in SUIT_TYPES) {
            const card = new Card({suit: SUIT_TYPES[suit], rank: parseInt(rankCount)});
            newDeck.push(card);
        }
    }
    const deck = new Deck({cards: newDeck, count: 52});
    deck.shuffle();
    return deck;
}
function initGame(){
    const generatedDeck = generateDeck();
    const firstPlayer = createPlayer('Artur', generatedDeck);
    const secondPlayer = createPlayer('Hanna', generatedDeck);

    Player.play(firstPlayer, secondPlayer);
}
function createPlayer(name, deck){
    return new Player({name: name, deck: deck, wins: 0, score: 0});
}
function startGame(generatedDeck, firstPlayer, secondPlayer){
    while(generatedDeck.count > 0) {
        gameRound(generatedDeck, firstPlayer, secondPlayer)
        generatedDeck.newCount = generatedDeck.cards.length;
    }
    compareScores(firstPlayer, secondPlayer);
}
function gameRound(deck, firstPlayer, secondPlayer) {
    let flippedCards = deck.draw(2);
    let winnerCard = Card.compare(flippedCards[0], flippedCards[1]);
    setPlayerScore(winnerCard, flippedCards,firstPlayer, secondPlayer);
}
function setPlayerScore(winnerCard, flippedCards,firstPlayer, secondPlayer){
    console.log(`${flippedCards[0].toString()} vs ${flippedCards[1].toString()}`);
    if(winnerCard !== 0) {
        winnerCard === flippedCards[0] ?
            firstPlayer.newScore = firstPlayer.score + 1 :
            secondPlayer.newScore = firstPlayer.score + 1;
    }
}
function compareScores(firstPlayer, secondPlayer) {
    if(firstPlayer.score === secondPlayer.score) {
        return showWinner();
    }
    if(firstPlayer.score > secondPlayer.score) {
        showWinner(firstPlayer, secondPlayer);
    } else {
        showWinner(secondPlayer, firstPlayer);
    }
}
function showWinner(winner = false, loser = false){
    if(winner && loser) {
        setWinner(winner);
        return console.log(`${winner.name} wins ${winner.score} to ${loser.score}`);
    } else {
        return console.log('Draw!');
    }
}
function setWinner(winner) {
    winner.win = winner.wins + 1;
}

initGame();


class Employee {

    constructor(options) {
        this.id = options.id
        this.firstName = options.firstName
        this.lastName = options.lastName
        this.birthday = options.birthday
        this.salary = options.salary
        this.position = options.position
        this.department = options.department
        Employee._employers.push(this);
    }

    static get EMPLOYEES() {
        return Employee._employers;
    }

    quit(){
        const employer = Employee._employers.indexOf(this);
        Employee._employers.splice(employer, 1);
    }

    retire() {
        console.log('It was such a pleasure to work with you!');
        this.quit();
    }

    getFired() {
        console.log('Not a big deal!')
        this.quit();
    }

    get age() {
        let ageDifMs = Date.now() - this.birthday.getTime();
        let ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }

    set changeDepartment(newDepartment) {
        this.department = newDepartment;
    }

    set changePosition(newPosition) {
        this.position = newPosition;
    }

    set changeSalary(newSalary) {
        this.salary = newSalary;
    }

    set getPromoted(benefits) {
        this.salary = benefits.salary ? benefits.salary : this.salary
        this.position = benefits.position ? benefits.position : this.position
        this.department = benefits.department ? benefits.department : this.department
        console.log('Yoohooo!');
    }
    set getDemoted(punishment) {
        this.salary = punishment.salary ? punishment.salary : this.salary
        this.position = punishment.position ? punishment.position : this.position
        this.department = punishment.department ? punishment.department : this.department
        console.log('Damn!');
    }
}
Employee._employers = [];

class Manager extends Employee {
    constructor(options) {
        super(options);
        this.position = 'manager';
    }

    get managedEmployees() {
        const managedEmployers = Employee.EMPLOYEES.filter(employer => {
            return employer.department === this.department && employer.position !== this.position;
        });
        return managedEmployers;
    }
}

class BlueCollarWorker extends Employee {}

class HRManager extends Manager {
    constructor(options) {
        super(options);
        this.department = 'hr';
    }
}

class SalesManager extends Manager {
    constructor(options) {
        super(options);
        this.department = 'sales';
    }
}

const manager = new Manager({
    id: 1,
    firstName: 'Artur',
    lastName: 'Verbin',
    birthday: new Date(2001, 2, 26),
    salary: 1000,
    department: 'hr'
});

const blueCollarWorker = new BlueCollarWorker({
    id: 3,
    firstName: 'Hanna',
    lastName: 'Verbin',
    birthday: new Date(2001, 2, 26),
    salary: 1000,
    department: 'hr',
    position: 'designer'
});

const hrManager = new HRManager({
    id: 2,
    firstName: 'Artur',
    lastName: 'Verbin',
    birthday: new Date(2001, 2, 26),
    salary: 1000,
    department: 'hr'
});

Object.assign(manager, {managerPro: managerPro});

function managerPro(){
    for(let employer = 0; employer < this.managedEmployees.length; employer++) {
        this.managedEmployees[employer].getPromoted = {salary: 10000};
    }
}