import startGameRound from './startGameRound';
import {resetGame} from './gameViews';

const resetBtn = document.querySelector('#reset');
export const userButtons = document.querySelectorAll('.btn');

const setListenersToButtons = () => {
    userButtons.forEach(button => {
        button.addEventListener('click', (el) => startGameRound(el.currentTarget));
    });

    resetBtn.addEventListener('click', () => resetGame());
};

export default setListenersToButtons;