import {userButtons} from './setListnersToButtons';

const resultBlock = document.querySelector('#result');
const gameProperties = {
    round: 0,
    userCount: 0,
    pcCount: 0
};

const resetGame = () => {
    resultBlock.innerHTML = '';
    gameProperties.userCount = gameProperties.pcCount = gameProperties.round = 0;
    setActive();
    setButtonsAttribute(false);
};

const showRoundWinner = (pc, user) => {
    const result = pc + ' vs ' + user;
    const pTag = document.createElement('p');
    pTag.classList.add('result__item');
    switch (result) {
        case 'rock vs scissors':
        case 'paper vs rock':
        case 'scissors vs paper':
            pTag.innerHTML = `
                Round ${gameProperties.round + 1},
                ${pc.charAt(0).toUpperCase() + pc.slice(1)} vs.
                ${user.charAt(0).toUpperCase() + user.slice(1)},
                You&rsquo;ve LOST!
            `;
            gameProperties.round++;
            gameProperties.pcCount += 1;
            resultBlock.append(pTag);
            break;

        case 'scissors vs rock':
        case 'rock vs paper':
        case 'paper vs scissors':
            pTag.innerHTML = `
                Round ${gameProperties.round + 1},
                ${user.charAt(0).toUpperCase() + user.slice(1)} vs.
                ${pc.charAt(0).toUpperCase() + pc.slice(1)},
                You&rsquo;ve WON!
            `;
            gameProperties.round++;
            gameProperties.userCount += 1;
            resultBlock.append(pTag);
            break;
        default:
            pTag.innerHTML = `Round ${gameProperties.round + 1}, Draw!`;
            gameProperties.round++;
            resultBlock.append(pTag);
    }
};

const setActive = () => {
    userButtons.forEach(btn => {
        btn.classList.remove('btn_active');
    });
};

const getGameWinner = () => {
    if(gameProperties.userCount === gameProperties.pcCount) {
        return `<span class="light-text">Draw!</span>`
    } else {
        return gameProperties.userCount > gameProperties.pcCount?
            `<span class="light-text">You</span> WON!`
            : `<span class="light-text">PC</span> WON!`;
    }
}

const setButtonsAttribute = (disable) => {
    if(disable) {
        userButtons.forEach(btn => {
            btn.setAttribute('disabled', `${disable}`);
        });
    } else {
        userButtons.forEach(btn => {
            btn.removeAttribute('disabled');
        });
    }
}


export {setActive,showRoundWinner,getGameWinner,resetGame,setButtonsAttribute,resultBlock,gameProperties};