import {setActive} from './gameViews';

const MAX_RANDOM_NUMBER = 3;
const MIN_RANDOM_NUMBER = 1;

export const PC_GESTURES = {
    1: 'rock',
    2: 'scissors',
    3: 'paper'
};


const getPCChoice = () => {
    const randomChoice = Math.floor(Math.random() * (MAX_RANDOM_NUMBER - MIN_RANDOM_NUMBER + MAX_RANDOM_NUMBER)
        + MIN_RANDOM_NUMBER);
    return PC_GESTURES[randomChoice];
};

const getUserChoice = (el) => {
    setActive();
    el.classList.add('btn_active');
    return el.id;
};

export {getPCChoice, getUserChoice};