import {getPCChoice,getUserChoice} from './gameRound';
import {showRoundWinner,setActive,getGameWinner,setButtonsAttribute,resultBlock,gameProperties} from './gameViews';
import {PC_GESTURES} from './gameRound';

const ROUND_TIMES = 3;

const startGameRound = (el) => {
    const pcChoice = getPCChoice(PC_GESTURES);
    const userChoice = getUserChoice(el);
    showRoundWinner(pcChoice, userChoice);
    if(gameProperties.round === ROUND_TIMES) {
        setActive();
        setButtonsAttribute(true);
        const result = document.createElement('h2');
        result.innerHTML = getGameWinner();
        resultBlock.append(result);
    }
};

export default startGameRound;