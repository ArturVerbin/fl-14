const getBiggestNumber = require('../src/get-bigest-number');

describe('get biggest number', () => {
    const functionResult = 5;

    it('should return max value', () => {
        expect(getBiggestNumber(1,2,3,4,5)).toBe(functionResult)
    });

    it('should throw error about wrong argument type', () => {
        const err = () => getBiggestNumber(1,2,'3',4)
        expect(err).toThrow();
    });

    it('should throw error about a small number of arguments', () => {
        const err = () => getBiggestNumber(1)
        expect(err).toThrow();
    });

    it('should throw error about a lot of arguments', () => {
        const err = () => getBiggestNumber(1,2,3,4,5,6,7,8,9,10,11)
        expect(err).toThrow();
    });
})