const BirthdayService = require('../src/birthday.service');

const service = new BirthdayService();

const birthDays = {
  first: {
      day: 26,
      month: 2,
      year: 2001
  },
  second: {
      day: 1,
      month: 11,
      year: 2001
  }
};

const todayBirthDay = 'Hurray !!! Today!';
const waitResult = 'Soon... Please, wait just 102 day/days';
const celebratedResult = 'Oh, you have celebrated it 13 day/s ago, don\'t you remember?';

describe('birthday-service', () => {
    const firstBirthDayDate = new Date(birthDays.first.year,birthDays.first.month,birthDays.first.day);
    const secondBirthDayDate = new Date(birthDays.second.year,birthDays.second.month,birthDays.second.day);

    it('should show message to wait', () => {
        return service.howLongToMyBirthday(firstBirthDayDate).then(result => {
            expect(result).toEqual(waitResult);
        });
    });

    it('should show message about already celebrated birthday', () => {
        return service.howLongToMyBirthday(secondBirthDayDate).then(result => {
            expect(result).toEqual(celebratedResult);
        });
    });

    it('should show message about today`s birthday', () => {
        return service.howLongToMyBirthday(new Date()).then(result => {
            expect(result).toEqual(todayBirthDay);
        });
    });

    it('should throw error', async () => {
        await expectAsync(service.howLongToMyBirthday('foo')).toBeRejectedWith('Wrong argument!');
    });
})