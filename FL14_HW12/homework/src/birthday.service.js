const DEFAULT_YEAR_DAYS = 365;
const SECONDS_PER_MINUTE = 60;
const MINUTES_PER_HOUR = 60;
const HOURS_PER_DAY = 24;
const MILLISECONDS_PER_SECOND = 1000;
const NUMBER_TO_HALVE = 2;

class BirthdayService {
    constructor() {
        this._hundredMiliseconds = 100;
        this.actualDate = new Date();
        this.actualYear = this.actualDate.getUTCFullYear();
        this.daysInYear = DEFAULT_YEAR_DAYS;
        this.millisecondsPerOneDay = MILLISECONDS_PER_SECOND
            * SECONDS_PER_MINUTE
            * MINUTES_PER_HOUR
            * HOURS_PER_DAY
        ;
    }
    howLongToMyBirthday(date) {
        return new Promise((resolve, reject) => {
            const birthDayDate = date;
            setTimeout(() => {
                if(date instanceof Date && !isNaN(birthDayDate.getTime())) {
                    if(
                        birthDayDate.getDate() === this.actualDate.getDate()
                        && birthDayDate.getMonth() === this.actualDate.getMonth()
                        && birthDayDate.getFullYear() === this.actualDate.getFullYear()
                    ) {
                        resolve(this.congratulateWithBirthday());
                    } else {
                        const nextBirthDay = new Date(
                            this.actualYear + 1,
                            birthDayDate.getMonth(),
                            birthDayDate.getDate()
                        );
                        this.actualDate.setHours(0,0,0,0);
                        birthDayDate.setHours(0,0,0,0);
                        this.birthDayMonth = date.getMonth();
                        this.birthDayDay = date.getDate();
                        const diff = nextBirthDay.getTime() - this.actualDate.getTime();
                        const daysTillBirthDay = Math.floor(diff / this.millisecondsPerOneDay);

                        resolve(this.notifyWaitingTime(daysTillBirthDay));
                    }
                } else {
                    reject('Wrong argument!');
                }
            }, this._hundredMiliseconds);
        });
    }

    congratulateWithBirthday = () => 'Hurray !!! Today!';

    notifyWaitingTime(time) {
        if(time <= this.daysInYear / NUMBER_TO_HALVE) {
            return `Soon... Please, wait just ${time} day/days`;
        } else {
            const lastBirthDayDate = new Date(
                this.actualYear,
                this.birthDayMonth,
                this.birthDayDay,
                0,0,0,0
            );
            const diff = this.actualDate.getTime() - lastBirthDayDate.getTime();
            const daysGoneBy = Math.floor(diff / this.millisecondsPerOneDay);
            return `Oh, you have celebrated it ${daysGoneBy} day/s ago, don't you remember?`;
        }
    }
}

module.exports = BirthdayService;