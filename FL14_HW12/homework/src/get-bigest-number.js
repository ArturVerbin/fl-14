const getBiggestNumber = (...array) => {
    const MAX_ARRAY_LENGTH = 10;
    const MIN_ARRAY_LENGTH = 2;
    if(array.length > MAX_ARRAY_LENGTH) {
        throw new Error('Too many arguments’');
    } else if(array.length < MIN_ARRAY_LENGTH) {
        throw new Error('Not enough arguments');
    }
    array.forEach(el => {
        if(typeof el !== 'number') {
            throw new Error('Wrong argument type');
        }
    });

    return Math.max(...array);

};
module.exports = getBiggestNumber;