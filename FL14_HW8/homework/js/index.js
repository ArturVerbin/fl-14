const $list = $(".list");
const $input = $("#add-input");
const $add = $("#add-submit");
const $search = $("#search-task");

const todos = [];

(function ($) {
  $.fn.createTodoItem = function (todos, currentTodo, saveToLocalStorage) {
    const getCurrentTodoIndex = () => this.parent().children().index(this);
    todos.push(currentTodo);
    $itemText = createTodoText(todos,currentTodo, this, getCurrentTodoIndex);
    $removeBtn = createRemoveButton(todos, this, getCurrentTodoIndex);
    this.append($itemText);
    if(currentTodo.done) {
      this.find('.item-text').addClass("done");
    }
    this.append($removeBtn);
    return this;
  };
  $.fn.isFits = function(text) {
    return this.find('.item-text').text().toLowerCase().includes(text);
  };
})(jQuery);

const todosFromLocalStorage = localStorage.getItem("todos")
    ? JSON.parse(localStorage.getItem("todos"))
    : [];

render();

$add.click((event)=> {
  event.preventDefault();
  if($input.val() !== '') {
    const newTodo = {
      text: $input.val(),
      done: false
    }
    $list.append(
        $('<li class="item"></li>').createTodoItem(todos, newTodo, saveToLocalStorage)
    );
    $input.val('');
    saveToLocalStorage();
  }
});

$search.keyup(function (event) {
  const searchInputTValue = event.target.value.trim();
  if (searchInputTValue.length === 0) {
    $list.children().show();
  } else {
    $list.children().each(function () {
      if ($(this).isFits(searchInputTValue)) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });
  }
});

function saveToLocalStorage() {
  localStorage.setItem("todos", JSON.stringify(todos));
}
function createTodoText(todos, currentTodo, context, getCurrentTodoIndex) {
  $itemText = $(`<span class="item-text">${currentTodo.text}</span>`);
  $itemText.click(() => {
    const itemIndex = getCurrentTodoIndex();
    todos[itemIndex].done = !todos[itemIndex].done;
    context.find('.item-text').toggleClass("done");
    saveToLocalStorage();
  });

  return $itemText;
}
function createRemoveButton(todos, context, getCurrentTodoIndex) {
  $removeBtn = $('<button class="item-remove">Remove</button>');
  $removeBtn.click(() => {
    const itemIndex = getCurrentTodoIndex();
    todos.splice(itemIndex, 1);
    context.remove();
    saveToLocalStorage();
  });
  return $removeBtn;
}
function render() {
  todosFromLocalStorage.forEach((todo) => {
    $list.append(
        $('<li class="item"></li>').createTodoItem(todos, todo, saveToLocalStorage)
    );
  });
}



