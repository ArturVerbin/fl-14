const API_URL = 'https://jsonplaceholder.typicode.com/users';
const NUMBERS_TO_SLICE = -4;
const NEGATIVE_NUMBER_ONE = -1;

function maxElement(array) {
    return Math.max(...array);
}

function copyArray(array) {
    const newArray = [...array];
    return newArray;
}

function addUniqueId(object) {
    const newObject = JSON.parse(JSON.stringify(object));
    newObject.id = Symbol('id');
    return newObject;
}

function regroupObject(object) {
    const {name: firstName, details:{ id, age, university}} = object;
    const newObject = {
        university,
        user: {
            age,
            firstName,
            id
        }
    };
    return newObject;
}

function finUniqueElements(array) {
    const result = Array.from(new Set(array));
    return result;
}

function hideNumber(number) {
    const lastFourNumbers = number.slice(NUMBERS_TO_SLICE);
    const result = lastFourNumbers.padStart(number.length, '*');
    return result;
}

function add(a = required(),b = required()) {
    return a + b;
}

function required() {
    throw new Error('Missing property');
}

function getNameOfUsers(url){
    const request = fetch( url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            return res.json();
        });
    request.then(usersList => {
        const sortedUserNames = usersList.map(user => user.name).sort((a,b) => a > b ? 1 : NEGATIVE_NUMBER_ONE);
        console.log(sortedUserNames);
    });
}

getNameOfUsers(API_URL);

async function getNameOfUsersAsyncAwait(url) {
    const response = await fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    });
    const userArray = await response.json();
    const sortedUserNames = userArray.map(user => user.name).sort((a,b) => a > b ? 1 : NEGATIVE_NUMBER_ONE);
    return sortedUserNames;
}

getNameOfUsersAsyncAwait(API_URL).then((data) => console.log(data));