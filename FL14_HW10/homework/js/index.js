const tab1Btn = document.getElementById('1');
const tab2Btn = document.getElementById('2');
const root = document.getElementById('root');

class ResourceManager{
    constructor(value) {
        this.value = value;
        this.pool = [];
    }

    addElement(el) {
        this.pool.push(el);
    }
}

class DefaultEmployee {
    constructor(value) {
        this.value = value;
    }
}

fetch('mock.json')
    .then(res => res.json())
    .then(res => {
        const employersTree = initTree(res);
        tab1Btn.addEventListener('click', () => renderEmployersList(employersTree));
        tab2Btn.addEventListener('click', () => renderUnits(employersTree.pool));
    })
    .catch(err => console.error(err));


function initTree(res) {
    const employees = [];
    res.forEach(item => {
        if (item['pool_name']) {
            employees.push(new ResourceManager(item))
        } else {
            employees.push(new DefaultEmployee(item))
        }
    });

    employees.forEach(item => {
        if (item.value['rm_id']) {
            employees[item.value['rm_id'] - 1].addElement(item);
        }
    });

    const tree = employees[0];

    return tree;
}

function getEmployersList(el) {
    let ul = document.createElement('ul');
    el.forEach(child => {
        let liTag = document.createElement('li');
        if(child.value['pool_name']) {
            let spanTag = document.createElement('span');
            spanTag.innerText = child.value.name;
            spanTag.classList.add('rm');
            liTag.append(spanTag,getEmployersList(child.pool));
        } else {
            liTag.innerText = child.value.name;
        }
        ul.append(liTag);
    });
    return ul;
}

function getUnits(el) {
    let ul = document.createElement('ul');
    el.forEach(unit => {
        let liTag = document.createElement('li');
        if(unit.value.performance === 'average') {
            liTag.innerText = unit.value.name;
            ul.append(liTag);
        }
        if(unit.value['pool_name']) {
            const newUl = getUnits(unit.pool);
            if(newUl.firstChild) {
             liTag.append(newUl);
             ul.append(liTag);
            }
        }
    });

    return ul;
}

function renderEmployersList(el) {
    root.innerHTML = '';
    const ulList = getEmployersList([el]);
    root.append(ulList);
}

function renderUnits(el) {
    root.innerHTML = '';
    const unitsList = getUnits(el);
    root.append(unitsList);
}