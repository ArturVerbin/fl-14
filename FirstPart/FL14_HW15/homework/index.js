/* START TASK 1: Your code goes here */
const simpleCells = document.querySelectorAll('.cell-block__item');
const specialCell = document.getElementById('special-cell');

simpleCells.forEach(el => {
    el.style.backgroundColor = 'white';
    el.addEventListener('click', event => cellPaint(event.currentTarget));
});

specialCell.addEventListener('click', () => {
    simpleCells.forEach(el => {
        if(el.style.backgroundColor === 'white') {
            el.style.backgroundColor = 'green';
        }
    });
});

function cellPaint(event) {
    const parent = event.parentNode;

    if(event === parent.children[0]) {
        for(let i = 0; i < parent.children.length; i++) {
            if(parent.children[i].style.backgroundColor !== 'yellow') {
                parent.children[i].style.backgroundColor = 'blue';
            }
        }
    } else {
        event.style.backgroundColor = 'yellow';
    }
}
/* END TASK 1 */

/* START TASK 2: Your code goes here */
const phoneInput = document.querySelector('.phone-validation__input');
const sendButton = document.querySelector('.phone-validation__button');
const resultBlock = document.querySelector('.phone-validation__result');
const phoneValidation = /^\+380[0-9]{9}$/;
const errorColor = 'red';
const errorMessage = 'Type number does not follow format +380*********';
const correctMessage = 'Data was successfully sent';
const correctColor = 'green';

phoneInput.addEventListener('input', event => validate(event));

sendButton.addEventListener('click', () => {
   if(phoneInput.value !== '') {
       showResult(correctMessage, correctColor);
   } else {
       showResult(errorMessage, errorColor);
   }
});
function validate(event){
    if(!event.target.value.match(phoneValidation)) {
        showResult(errorMessage, errorColor);
        sendButton.setAttribute('disabled', 'true');
        phoneInput.classList.add('phone-validation__input_invalid');
        phoneInput.classList.remove('phone-validation__input_valid');
    } else {
        resultBlock.classList.remove('phone-validation__result_opened');
        sendButton.removeAttribute('disabled');
        phoneInput.classList.remove('phone-validation__input_invalid');
        phoneInput.classList.add('phone-validation__input_valid');
    }
}
function showResult(resultMessage, resultBg) {
    resultBlock.classList.add('phone-validation__result_opened');
    resultBlock.textContent = resultMessage;
    resultBlock.style.backgroundColor = resultBg;
}
/* END TASK 2 */

/* START TASK 3: Your code goes here */
const ball = document.querySelector('.basketball-court__ball');
const basketBallCourt = document.querySelector('.basketball-court');
const teamA = document.getElementById('first-player');
const teamB = document.getElementById('second-player');
const gameResult = document.querySelector('.basketball__event');
let teamAScore = 0;
let teamBScore = 0;
const RESET_TIMEOUT = 3000;
const leftGoalSquare = {
    x1: 30,
    x2: 45,
    y1: 142,
    y2: 158
};
const rightGoalSquare = {
    x1: 545,
    x2: 560,
    y1: 142,
    y2: 158
};

startGame();
basketBallCourt.addEventListener('click', event => moveBallToPosition(event));

function startGame(){
    moveBallToCenter();
    renderTeamScore(teamAScore, teamBScore);
}
function moveBallToCenter(){
    ball.style.top = '50%';
    ball.style.left = '50%';
}
function renderTeamScore(firstTeam, secondTeam){
    teamA.textContent = `Team A: ${firstTeam}`;
    teamB.textContent = `Team B: ${secondTeam}`;
}
function moveBallToPosition(event) {
    ball.style.top = event.offsetY + 'px';
    ball.style.left = event.offsetX + 'px';
    if(event.offsetY >= leftGoalSquare.y1 && event.offsetY <= leftGoalSquare.y2) {
        if(event.offsetX >= leftGoalSquare.x1 && event.offsetX <= leftGoalSquare.x2) {
            teamBScore += 1;
            teamB.textContent = `Team B: ${teamBScore}`;
            showTeamResult('B', 'red');
        } else if (event.offsetX >= rightGoalSquare.x1 && event.offsetX <= rightGoalSquare.x2) {
            teamAScore += 1;
            teamA.textContent = `Team A: ${teamAScore}`;
            showTeamResult('A', 'blue');
        }
    }
}
function showTeamResult(team, textColor){
    basketBallCourt.style.pointerEvents = 'none';
    gameResult.style.visibility = 'visible';
    gameResult.style.color = textColor;
    gameResult.textContent = `Team ${team} score!`;
    setTimeout(() => {
        gameResult.style.visibility = 'hidden';
        moveBallToCenter();
        basketBallCourt.style.removeProperty('pointer-events');
    }, RESET_TIMEOUT);
}
/* END TASK 3 */
