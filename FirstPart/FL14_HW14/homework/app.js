const data = [
  {
    'folder': true,
    'title': 'Grow',
    'children': [
      {
        'title': 'logo.png'
      },
      {
        'folder': true,
        'title': 'English',
        'children': [
          {
            'title': 'Present_Perfect.txt'
          }
        ]
      }
    ]
  },
  {
    'folder': true,
    'title': 'Soft',
    'children': [
      {
        'folder': true,
        'title': 'NVIDIA',
        'children': null
      },
      {
        'title': 'nvm-setup.exe'
      },
      {
        'title': 'node.exe'
      }
    ]
  },
  {
    'folder': true,
    'title': 'Doc',
    'children': [
      {
        'title': 'project_info.txt'
      }
    ]
  },
  {
    'title': 'credentials.txt'
  }
];

const rootNode = document.getElementById('root');
const contextMenu = createContextMenu();
const folderBlock = renderFolderBlock();
let contextTarget;
folderBlock.append(contextMenu);
renderCategory(data, folderBlock);

const folderBlocks = document.querySelectorAll('.folder-block__content_folder');
const folderItems = document.querySelectorAll('.folder-block__item');
const itemContent = document.querySelectorAll('.folder-block__content');
const renameButton = document.querySelector('.rename');
const deleteButton = document.querySelector('.delete');

folderBlock.addEventListener('contextmenu', event => {
  event.preventDefault();
  const contextBlock = document.querySelector('.context-menu');
  contextBlock.style.top = event.y + 'px';
  contextBlock.style.left = event.x + 'px';
  contextBlock.style.display = 'flex';
  for(let i = 0; i < contextBlock.children.length; i++) {
    contextBlock.children[i].disabled = true;
  }
});

folderBlocks.forEach(el => {
  el.addEventListener('click', event => folderClick(event));
});

itemContent.forEach(el => {
  el.addEventListener('contextmenu', event => {
    event.stopPropagation();
    event.preventDefault();
    const contextBlock = document.querySelector('.context-menu');
    contextBlock.style.top = event.y + 'px';
    contextBlock.style.left = event.x + 'px';
    contextBlock.style.display = 'flex';
    for(let i = 0; i < contextBlock.children.length; i++) {
      contextBlock.children[i].disabled = false;
    }
    contextTarget = event.currentTarget;
  });
});

folderItems.forEach(el => {
  el.addEventListener('click', () => {
    const contextBlock = document.querySelector('.context-menu');
    contextBlock.style.display = 'none';
  });
});

renameButton.addEventListener('click', () => {
  contextTarget.children[1].setAttribute('contenteditable', 'true');
  const contextBlock = document.querySelector('.context-menu');
  contextBlock.style.display = 'none';
});

deleteButton.addEventListener('click', () => {
  const parent = contextTarget.parentNode;
  console.log(parent)
  contextTarget.remove();
  if(parent.children.length === 0) {
    const empty = document.createElement('li');
    empty.textContent = 'Folder is empty';
    parent.append(empty);
  }
  const contextBlock = document.querySelector('.context-menu');
  contextBlock.style.display = 'none';
});


function createContextMenu() {
  const contextMenu = document.createElement('div');
  contextMenu.className = 'context-menu';
  const contextDelete = document.createElement('button');
  contextDelete.className = 'delete';
  contextDelete.textContent = 'Delete item';
  const contextEdit = document.createElement('button');
  contextEdit.className = 'rename';
  contextEdit.textContent = 'Rename';
  const hr = document.createElement('hr');

  contextMenu.append(contextEdit, hr, contextDelete);
  return contextMenu;
}
function folderClick(event){
  event.stopPropagation();
  const folder = event.currentTarget.parentNode;
  for (let i = 0; i < folder.children.length; i++) {
    if(folder.children[i].className === 'folder-block__children open') {
      folder.children[i].className = 'folder-block__children';
      changeIcon('folder', event);
    } else if (folder.children[i].className === 'folder-block__children') {
      folder.children[i].className = 'folder-block__children open';
      changeIcon('folder_open', event);
    }
  }
}
function renderFolderBlock(){
  const folderBlock = document.createElement('ul');
  folderBlock.className = 'folder-block';
  rootNode.append(folderBlock);
  return folderBlock;
}
function renderCategory(folderArray, placeToRender) {
  folderArray.forEach(el => {
    if(el.hasOwnProperty('folder')) {
      const newFolder = createFolderBlockItem(el.title, 'folder', 'folder', 'folder-item');
      placeToRender.append(newFolder);
      if (el.hasOwnProperty('children') && el.children !== null) {
        const childFolder = document.createElement('ul');
        childFolder.className = 'folder-block__children';
        newFolder.append(childFolder);
        renderCategory(el.children, childFolder);
      } else {
        const emptyFolder = document.createElement('ul');
        emptyFolder.className = 'folder-block__children';
        const empty = document.createElement('li');
        empty.textContent = 'Folder is empty';
        emptyFolder.append(empty);
        newFolder.append(emptyFolder);
      }
    } else {
      const newFile = createFolderBlockItem(el.title, 'insert_drive_file', 'file', 'file-item');
      placeToRender.append(newFile);
    }
  })
}
function createFolderBlockItem(elementText,imgName, imgClass, elementClass) {
  const element = document.createElement('li');
  const elementContent = document.createElement('div');
  const elementTextContent = document.createElement('p');
  const elementImg = document.createElement('span')
  elementImg.className = `material-icons ${imgClass}`;
  element.className = `folder-block__item ${elementClass}`;
  elementImg.textContent = imgName;
  elementTextContent.setAttribute('contenteditable', 'false');
  elementTextContent.textContent = elementText;
  elementContent.className = `folder-block__content folder-block__content_${imgClass}`;
  elementContent.append(elementImg, elementTextContent);
  element.append(elementContent);
  return element;
}
function changeIcon(imgClass, event) {
  const newSpan = document.createElement('span');
  const nameOfFolder = document.createElement('p');
  newSpan.textContent = `${imgClass}`;
  newSpan.className = 'material-icons folder';
  nameOfFolder.textContent = event.currentTarget.lastChild.textContent;
  nameOfFolder.setAttribute('contenteditable', 'false');
  event.currentTarget.innerHTML = '';
  event.currentTarget.append(newSpan,nameOfFolder);
}