const NUM_OF_POINTS = 3;
function isEqual(firstNum,secondNum){
    return firstNum === secondNum;
}
function numberToString(number) {
    return number.toString();
}
function storeNames(...array) {
    return array;
}
function getDivision(firstNum,secondNum){
    return firstNum < secondNum ? secondNum / firstNum : firstNum / secondNum;
}
function negativeCount(array){
    let negativeNumCounter = array.reduce((count, current) => {
        if(current < 0) {
            return count + 1;
        } else {
            return count;
        }
    }, 0);
    return negativeNumCounter;
}
function letterCount(string, letter){
    let letterCounter = string.split('').reduce((count,current) => {
        if(current === letter) {
            return count + 1;
        } else {
            return count;
        }
    },0);
    return letterCounter;
}
function countPoints(scoresList) {
    let result = scoresList.reduce((count, current) => {
        const score = current.split(':');
        const myScore = parseInt(score[0]);
        const enemyScore = parseInt(score[1]);
        if(myScore > enemyScore) {
            return count + NUM_OF_POINTS;
        } else if (myScore === enemyScore) {
            return count + 1;
        } else {
            return count;
        }
    }, 0);
   return result;
}