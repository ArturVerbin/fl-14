const root = document.getElementById('root');
const bookList = JSON.parse(localStorage.getItem('books'));
const routes = [
    { path: undefined, component: renderList},
    { path: 'preview', component: renderPreview},
    { path: 'edit', component: renderEdit},
    { path: 'add'}
];


router();

window.addEventListener('popstate', () => {
    router();
});

function router(){
    const parsedUrl = new URL(window.location.href);
    const itemId = parseInt(parsedUrl.searchParams.get('id')) || null;
    const path = parseLocation();
    const component = findComponentByPath(path, routes) || routes[0];
    root.innerHTML = '';
    root.append(component.component(itemId))
}
function findComponentByPath(path, routes){
    return routes.find(r => r.path === path) || undefined;
}
function parseLocation() {
    return location.hash.slice(1).toLowerCase() || '/';
}
function linkTo(id, hash) {
    const base = location.href;
    const url = `index.html` + (hash ? `?id=${id}#${hash}` : '');
    const newUrl = new URL(url, base);
    history.pushState(null, null, `${newUrl}`);
    router();
}
function renderBookList(placeToRender) {
    placeToRender.innerHTML = '';
    bookList.forEach(el => {
       const bookItem = document.createElement('div');
       const bookName = document.createElement('a');
       const editButton = document.createElement('button');
       editButton.setAttribute('data-id', `${el.id}`);
       bookItem.classList.add('book-list__item');
       editButton.textContent = 'edit';
       bookName.setAttribute('href', `./`);
       bookName.setAttribute('id', `${el.id}`);
       bookName.textContent = el.name;
       bookItem.append(bookName, editButton);
       placeToRender.append(bookItem);
       bookName.addEventListener('click', event => {
           event.preventDefault();
           linkTo(event.target.id, 'preview');
       })
       editButton.addEventListener('click', event => {
           linkTo(event.target.dataset.id, 'edit');
       })
    });
    const addButton = document.createElement('button');
    addButton.textContent = 'add';
    placeToRender.append(addButton);
}
function renderAppTemplate(dynamicRouteBlock = '') {
    const mainBlock = document.createElement('div');
    mainBlock.classList.add('main-block');
    const bookListBlock = document.createElement('div');
    const dynamicBlock = document.createElement('div');
    dynamicBlock.classList.add('dynamic-block');
    bookListBlock.classList.add('book-list');
    dynamicBlock.append(dynamicRouteBlock);
    renderBookList(bookListBlock);
    mainBlock.append(bookListBlock, dynamicBlock);
    return mainBlock;
}
function renderPreview(id) {
    const book = bookList.find(el => el.id === id);
    const previewBlock = createElement('div','preview-block');
    const previewImg = createElement('div','preview-block__img');
    const previewContent = createElement('div','preview-block__content');
    const bookHeading = createElement('h2', 'preview-block__title', book.name);
    const bookAuthor = createElement('p', 'preview-block__author', book.author);
    const bookPlot = createElement('p', 'preview-block__plot', book.plot);
    const bookImg = createElement('img', 'preview-block__book-img', book.img);
    previewImg.append(bookImg);
    previewContent.append(bookHeading, bookAuthor, bookPlot);
    previewBlock.append(previewImg,previewContent);

    return renderAppTemplate(previewBlock);
}
function createElement(tagName, tagClass, tagContent = '') {
    const newElement = document.createElement(`${tagName}`);
    if(tagName === 'img') {
        newElement.setAttribute('src', `${tagContent}`);
    } else {
        newElement.textContent = tagContent;
    }
    newElement.classList.add(`${tagClass}`);
    return newElement;
}
function renderEdit(id) {
    const book = bookList.find(el => el.id === id);
    const form = `
        <form>
            <div>
               <label for="name">Name</label>
                <input type="text" id="name" required value="${book.name}">         
            </div>
            <div>
                <label for="author">Author</label>
                <input type="text" id="author" required value="${book.author}">
            </div>     
            <div>
                <label for="plot">Plot</label>
                <textarea required id="plot" rows="10">${book.plot}</textarea>
            </div>
            <div>
                <label for="img">Img</label>
                <input type="url" required id="author" value="${book.img}">
            </div>     
            <button id="save">Save</button>
            <button id="cancel" onclick="cancelEdit()">Cancel</button>
        </form>
    `;
    const formBlock = document.createElement('div');
    formBlock.innerHTML = form;
    return renderAppTemplate(formBlock);
}
function renderList() {
   return renderAppTemplate();
}
function cancelEdit(){
    const isCanceled = confirm('Discard Changes?');
    if(isCanceled) {
       linkTo();
    }
}