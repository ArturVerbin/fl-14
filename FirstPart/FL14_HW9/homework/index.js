const SIMPLE_NUMBER = 9;
// Function 1
function convert(...array) {
    for (let i = 0; i < array.length; i++) {
        typeof array[i] === 'number' ? array[i] = array[i].toString() : array[i] = parseInt(array[i]);
    }
    return array;
}
// Function 2
function executeForEach(array, lambdaFunction) {
    let newArray = [];
    for (let i = 0; i < array.length; i++) {
        newArray.push(lambdaFunction(array[i]));
    }
    return newArray;
}
// Function 3
function mapArray(array, callback) {
    const numberArray = [];
    for(let i = 0; i < array.length; i++) {
        numberArray.push(parseInt(array[i]));
    }
    return executeForEach(numberArray, callback);
}
// Function 4
function filterArray(array, callback) {
    const filteredIds = executeForEach(array,callback);
    const filteredArray = [];
    for(let i = 0; i < array.length; i++){
        if(filteredIds[i]) {
            filteredArray.push(array[i]);
        }
    }
    return filteredArray;
}
// Function 5
function getValuePosition(array, searchNumber) {
    let arrayIndex;
    for (let i = 0; i < array.length; i++) {
        if (array[i] === searchNumber) {
            arrayIndex = i;
            return arrayIndex + 1;
        }
    }
    return false;
}
// Function 6
function flipOver(string) {
    let reversed = '';
    for(let i = string.length - 1; i >= 0; i--){
        reversed += string[i];
    }
    return reversed;
}
// Function 7
function makeListFromRange(array) {
    const newArray = [];
    for(let i = array[0]; i <= array[1]; i++) {
        newArray.push(i);
    }
    return newArray;
}
// Function 8
function getArrayOfKeys(object, keyName) {
    return executeForEach(object, function(el) {
 return el[keyName] 
});
}
// Function 9
function getTotalWeight(object) {
    const weightArray = executeForEach(object, function (el) {
 return el.weight 
});
    let result = 0;
    for(let i = 0; i < weightArray.length; i++) {
        result += weightArray[i];
    }
    return result;
}
// Function 10
function getPastDay(date, day) {
    let newDate = date;
    newDate.setDate(date.getDate() - day);
    const monthNames = [
        'Jan', 'Feb', 'Mar',
        'Apr', 'May', 'Jun', 'Jul',
        'Aug', 'Sep', 'Oct',
        'Nov', 'Dec'
    ];
    const dateDay = newDate.getDate();
    const dateMonth = newDate.getMonth();
    const dateYear = newDate.getFullYear();
    return dateDay + ', (' + dateDay + ' ' + monthNames[dateMonth] + ' ' + dateYear + ')';
}
// Function 11
function formatDate(date) {
    let dateDay = makeTwoDigitNumber(date.getDate());
    let dateMonth = makeTwoDigitNumber(date.getMonth() + 1);
    let dateYear = date.getFullYear();
    let dateMinutes = makeTwoDigitNumber(date.getMinutes());
    let dateHour = makeTwoDigitNumber(date.getHours());

    let result = dateYear + '/' + dateMonth + '/' + dateDay + ' ' + dateHour + ':' + dateMinutes;
    return result;
}
// Function 12
function makeTwoDigitNumber(value) {
    return value <= SIMPLE_NUMBER ? '0' + value : value;
}