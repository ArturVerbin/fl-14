const getResult = firstTry => {
    try {
        let result = 0;
        result = eval(getExpression(firstTry));
        if(isNaN(result)) {
            const nanError = new Error('Result is not a number');
            throw nanError;
        } else if (result === Infinity) {
            const infinityError = new Error('Result is infinite');
            throw infinityError;
        }
        alert(result);
    } catch(e) {
        alert(e.message);
        return getResult(false);
    }
}
getResult(true);
function getExpression(firstTry) {
    if(firstTry) {
        return prompt('Enter your expression:');
    } else {
        return prompt('Enter valid expression:')
    }
}