function visitLink(path) {
    let visitsCount = 0;
    if(localStorage.getItem(path)) {
        visitsCount = parseInt(localStorage.getItem(path));
    }
    localStorage.setItem(path, `${visitsCount + 1}`);
}

function viewResults() {
    if(localStorage.length) {
        const container = document.querySelector('.mx-auto');
        const ul = document.createElement('ul');
        container.append(ul);
        for(let i = 0; i < localStorage.length; i++) {
            const path = localStorage.key(i),
                countCLicks = localStorage.getItem(path),
                li = document.createElement('li');

            li.textContent = `You visited ${path} ${countCLicks} time(s)`;
            ul.append(li);
        }
        localStorage.clear();
    }
}