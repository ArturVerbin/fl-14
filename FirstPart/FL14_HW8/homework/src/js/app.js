const startBtn = document.getElementById('start'),
        restartBtn = document.getElementById('restart'),
        skipBtn = document.getElementById('skip'),
        welcomeBlock = document.getElementById('welcome'),
        gameBlock = document.getElementById('game'),
        resultBlock = document.getElementById('result'),
        answers = document.querySelectorAll('.questions__item'),
        totalPrizeBlock = document.getElementById('total'),
        roundPrizeBlock = document.getElementById('prize'),
        questionBlock = document.getElementById('heading'),
        resultTextBLock = document.getElementById('resultText'),
        WINNER_PRIZE = 1000000,
        NEXT_ROUND_RATIO = 2,
        INITIAL_ROUND_PRIZE = 100,
        gameQuestions = JSON.parse(localStorage.getItem('qArray'));
let currentQuestionIndex;
let questionList;
let roundPrize,totalPrize;

init();

startBtn.addEventListener('click', () => {
    startGame();
});

restartBtn.addEventListener('click', () => {
    startGame();
});

skipBtn.addEventListener('click', skipQuestion);

function init() {
    answers.forEach(el => {
        el.addEventListener('click', e => selectAnswer(e.target.id));
    });
}
function startGame() {
    roundPrize = INITIAL_ROUND_PRIZE;
    totalPrize = 0;
    skipBtn.style.display = 'block';
    resultBlock.style.display = 'none';
    welcomeBlock.style.display = 'none';
    gameBlock.style.display = 'flex';
    questionList = getQuestionsList(gameQuestions);
    currentQuestionIndex = 0;
    console.log(questionList);
    totalPrizeBlock.innerText = totalPrize;
    roundPrizeBlock.innerText = roundPrize;
    setNewQuestion(questionList, currentQuestionIndex);
}
function setNewQuestion(array, index) {
    showQuestion(array[index]);
}
function showQuestion(question){
    questionBlock.innerText = question.question;
    showAnswers(question.content);
    console.log(question.correct);
}
function showAnswers(answerList) {
    for (let counter = 0; counter < answers.length; counter++) {
        answers[counter].innerHTML = answerList[counter];
    }
}
function selectAnswer(id){
    if (parseInt(id) === questionList[currentQuestionIndex].correct) {
        totalPrize += roundPrize;
        totalPrizeBlock.innerText = totalPrize;
        roundPrize *= NEXT_ROUND_RATIO;
        roundPrizeBlock.innerText = roundPrize;
        currentQuestionIndex++;
        if(totalPrize >= WINNER_PRIZE) {
            gameBlock.style.display = 'none';
            resultBlock.style.display = 'flex';
            resultTextBLock.innerText = `Congratulations!\n You won ${WINNER_PRIZE}`;
        } else {
            setNewQuestion(questionList, currentQuestionIndex);
        }
    } else {
        gameBlock.style.display = 'none';
        resultBlock.style.display = 'flex';
        resultTextBLock.innerText = `Game Over!\n Your prize ${totalPrize}`;
    }
}
function getQuestionsList(array){
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}
function skipQuestion() {
    currentQuestionIndex++;
    setNewQuestion(questionList,currentQuestionIndex);
    skipBtn.style.display = 'none';
}