// Task 1
function getAge(userBirthday) {
    const GOlDEN_YEAR = 1970;
    let todayDate = new Date();
    let userMillisecondsAge = new Date(todayDate - userBirthday.getTime());
    return Math.abs(userMillisecondsAge.getUTCFullYear() - GOlDEN_YEAR);
}
// Task 2
function getWeekDay(userDate) {
    return new Date(userDate).toLocaleString('en', {weekday: 'long'});
}
// Task 3
function getProgrammersDay(userYear) {
    const programmerDay = 256;
    const date = new Date(userYear, 0, programmerDay);

    const dateDay = date.getDate();
    const dateMonth = date.toLocaleString('en', {month: 'short'});

    return `${dateDay} ${dateMonth}, ${userYear} (${getWeekDay(date)})`;
}
// Task 4
function howFarIs(dayOfWeek) {
    const userDay = dayOfWeek[0].toUpperCase() + dayOfWeek.slice(1);
    const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const actualTime = new Date();
    const today = weekDays[actualTime.getDay()];
    const indexOfToday = weekDays.indexOf(today);
    const indexOfUserDay = weekDays.indexOf(userDay);

    if(indexOfToday > indexOfUserDay) {
        return `It's ${indexOfUserDay + (weekDays.length - indexOfToday)} day(s) till ${userDay}`;
    } else if (indexOfUserDay > indexOfToday){
        return `It's ${indexOfUserDay - indexOfToday} day(s) till ${userDay}`;
    } else if (indexOfToday === indexOfUserDay) {
        return `Hey, today is ${userDay}`;
    }
}
// Task 5
function isValidIdentifier(string) {
    const stringValidation = /^[a-zA-Z_$][a-zA-Z_$0-9]*$/;
    let result = stringValidation.test(string);
    return result;
}
// Task 6
function capitalize(string) {
    const validationRegex = /^(.)|\s+(.)/g;
    return string.replace(validationRegex, letter => letter.toUpperCase());
}
// Task 7
function isValidAudioFile(fileName) {
    const fileValidation = /^[a-zA-Z]*\.(mp3|flac|alac|aac)$/;
    let result = fileValidation.test(fileName);
    return result;
}
// Task 8
function getHexadecimalColors(userColors) {
    const hexValidation = /#[a-fA-F0-9]{6}\b|#[a-fA-F0-9]{3}\b/g;
    return userColors.match(hexValidation) === null ? [] : userColors.match(hexValidation);
}
// Task 9
function isValidPassword(userPassword) {
    const passwordValidation = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8})/;

    let result = passwordValidation.test(userPassword);

    return result;
}
// Task 10
function addThousandsSeparators(userNumber) {
    const numberSeparator = /\B(?=(\d{3})+(?!\d))/g;
    return userNumber.toString().replace(numberSeparator, ',');
}