let result;
const TWO = 2;
let userData = prompt('Enter your word:').trim();

if(userData === ' ' || !userData.trim()){
    alert('Invalid value');
} else {
    if(userData.length % TWO === 0) {
        result = userData.substr(userData.length / TWO - 1, TWO);
        if(result[0] === result[1]) {
            alert('Middle characters are the same');
        } else {
            alert(result);
        }
    } else {
        result = userData.charAt(userData.length / TWO);
        alert(result);
    }
}