let userAmountBatteries = parseInt(prompt('Input amount of batteries:'));
let userDefBatteries = parseInt(prompt('Input percentage of defective batteries:'));
const HUNDRED = 100;

if(userAmountBatteries >= 0 && userDefBatteries >= 0 && userDefBatteries <= HUNDRED) {
    let defBatteries = parseFloat(amountDef(userAmountBatteries, userDefBatteries));
    let workingBatteries = parseFloat(amountWorking(userAmountBatteries, defBatteries));
    alert(`
        Amount of batteries: ${userAmountBatteries}
        Defective rate: ${userDefBatteries}%
        Amount of defective batteries: ${defBatteries}
        Amount of working batteries: ${workingBatteries}
    `);
} else {
    alert('Invalid input data!');
}

function amountDef(amount, def) {
    return def * amount / HUNDRED;
}
function amountWorking(amount,def) {
    return amount - def;
}