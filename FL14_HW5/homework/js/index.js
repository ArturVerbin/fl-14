const API_URL = 'https://jsonplaceholder.typicode.com/';
const bodyBlock = document.querySelector('body');
const rootBlock = document.createElement('div');

init();
const spinner = createSpinner();
render();

function init() {
    rootBlock.setAttribute('id', 'root');
    bodyBlock.append(rootBlock);
    const styles = `
    html,body {
        height: 100%;
    }
    body {
        position: relative;
    }
    label {
        display: block;
    }
    .name {
        border: none;
        outline: none;
        text-decoration: underline;
        background: transparent;
        cursor: pointer;
    }
    .spinner {
      display: 'none';
      width: 40px;
      height: 40px;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      position: absolute;
    }

    .double-bounce1, .double-bounce2 {
      width: 100%;
      height: 100%;
      border-radius: 50%;
      background-color: #333;
      opacity: 0.6;
      position: absolute;
      top: 0;
      left: 0;
      
      -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
      animation: sk-bounce 2.0s infinite ease-in-out;
    }

    .double-bounce2 {
      -webkit-animation-delay: -1.0s;
      animation-delay: -1.0s;
    }

    @-webkit-keyframes sk-bounce {
      0%, 100% { -webkit-transform: scale(0.0) }
      50% { -webkit-transform: scale(1.0) }
    }
    
    @keyframes sk-bounce {
      0%, 100% { 
        transform: scale(0.0);
        -webkit-transform: scale(0.0);
      } 50% { 
        transform: scale(1.0);
        -webkit-transform: scale(1.0);
      }
    }
`;
    document.head.appendChild(document.createElement('style')).textContent = styles;
}
function createSpinner() {
    const spinnerBlock = document.createElement('div');
    spinnerBlock.setAttribute('id', 'spinner');
    spinnerBlock.className = 'spinner';
    const spinnerCircles = `
      <div class="double-bounce1"></div>
      <div class="double-bounce2"></div>
    `;
    spinnerBlock.innerHTML = spinnerCircles;
    bodyBlock.append(spinnerBlock);
    return spinnerBlock;
}
function sendRequest(method, url, body = null) {
    const headers = {
        'Content-type': 'application/json'
    }
    rootBlock.innerHTML = '';
    spinner.style.display = 'block';
    return fetch(API_URL + url, {
        method,
        body: body ? JSON.stringify(body) : null,
        headers: headers
    })
        .then(response => {
            if(response.ok) {
                spinner.style.display = 'none';
                return response.json();
            }

            return response.json().then(error => {
                const e = new Error('Something went wrong!');
                e.data = error;
                throw e;
            });
        });
}
function render() {
    sendRequest('GET', 'users')
        .then(data => {
            data.forEach(user => {
                const createdUser = createUser(user);
                displayElement(createdUser);
            });
        })
        .catch(err => {
            console.log(err);
        });
}
function createUser(user) {
    const userBlock = document.createElement('div');
    const newUser = `
        <ul data-id="${user.id}">
            <button class="edit" data-id="${user.id}">Edit</button>
            <button class="delete" data-id="${user.id}">Delete</button>
            <li>
                Name:
                <button class="name">${user.name}</button>
            </li>
            <li>
                Phone: ${user.phone}
            </li>
            <li>
                Website: ${user.website}
            </li>
            <li>
                Email: ${user.email}
            </li>
        </ul> 
    `;
    userBlock.innerHTML = newUser;
    const nameButton = userBlock.getElementsByClassName('name');
    const editButton = userBlock.getElementsByClassName('edit');
    const deleteButton = userBlock.getElementsByClassName('delete');
    deleteButton[0].addEventListener('click', el => deleteUser(el.target));
    editButton[0].addEventListener('click', el => editInput(el.target, user));
    nameButton[0].addEventListener('click', el => getPost(el, user.id));
    return userBlock;
}
function displayElement(user) {
    rootBlock.append(user);
}
function editInput(el, user) {
    const ulElements = el.parentNode.getElementsByTagName('li');
    for(let li = ulElements.length - 1; li >= 0; li--) {
        ulElements[li].remove();
    }
    el.setAttribute('disabled', 'disabled');
    const createdForm = createUpdateForm(user);
    el.parentNode.append(createdForm);
}
function createUpdateForm(user) {
    const updateForm = document.createElement('form');
    const formContent = `
        <label for="name">Name:</label>
        <input type="text" id="name" value="${user.name}">
        <label for="phone">Phone:</label>
        <input type="text" id="phone" value="${user.phone}">
        <label for="website">Website:</label>
        <input type="text" id="website" value="${user.website}">
        <label for="email">Email:</label>
        <input type="text" id="email" value="${user.email}">
        <button type="submit" class="update">Update</button>
    `;
    updateForm.innerHTML = formContent;
    const updateButton = updateForm.getElementsByClassName('update');
    updateButton[0].addEventListener('click', el => updateUserInfo(el, user.id));
    return updateForm;
}
function updateUserInfo(el, id){
    el.preventDefault();
    const body = {};
    const parentElement = el.target.parentNode;
    const inputValues = parentElement.getElementsByTagName('input');
    for(let li = inputValues.length - 1; li >= 0; li--) {
        body[inputValues[li].id] = inputValues[li].value
    }
    sendRequest('PUT', `users/${id}`, body)
        .then(() => {
            render();
        })
}
function deleteUser(element){
    const id = element.dataset.id;
    sendRequest('DELETE', `users/${id}`)
        .then(() => {
            render();
        });
}
function getPost(el, id){
    el.preventDefault();
    Promise.all([
        sendRequest('GET', `posts?userId=${id}`),
        sendRequest('GET', `comments?postId=${id}`)
    ]).then(response => {
        response[0].forEach(post => {
            const createdPost = createPost(post.body, post.title);
            const commentsTitle = document.createElement('h3');
            commentsTitle.innerText = 'Comments:';
            createdPost.append(commentsTitle);
            response[1].forEach(comment => {
                const createdComment = createComment(comment.name, comment.email, comment.body);
                createdPost.append(createdComment);
            });

            rootBlock.append(createdPost);
        })
    });
}
function createPost(body,title){
    const userPost = document.createElement('div');
    const newPost = `
        <h2>Post name: ${title}</h2>
        <p>${body}</p>
    `;
    userPost.innerHTML = newPost;
    return userPost;
}
function createComment(name,email,body) {
    const postComment = document.createElement('div');
    const newComment = `
        <h4>User Name: ${name} <span>User Email: ${email}</span></h4>
        <p>${body}</p>
    `;
    postComment.innerHTML = newComment;
    return postComment;
}