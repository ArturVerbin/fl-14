import Board from './Board';
import TicTacToe from './TicTacToe';

export default class TicTacToeController {
    constructor() {
        this.squareBtns = document.querySelectorAll('.grid__item');
        this.infoBlock = document.querySelector('.info');
        this.playerBlock = document.querySelector('.player');
        this.resetBtn = document.getElementById('reset-game');
        this.newGameBtn = document.getElementById('new-game');
        this.resultBlock = document.querySelector('.result');
        const board = new Board({
            squareCount: 9,
            callback: this.onBoardChange.bind(this)
        });
        this.game = new TicTacToe({
            board,
            onGameEnd: this.onGameEnd.bind(this)
        });
        this.initEvents();
    }

    handleClick(event) {
        const player = this.game.getCurrentPlayer();
        this.game.setSquareValue(parseInt(event.dataset.id), player.mark);
        this.showRoundPlayer();
    }

    onBoardChange(squaresArray) {
        this.squareBtns.forEach((square,idx) => {
           square.innerHTML = squaresArray[idx];
        });
    }

    startGame() {
        this.game.initPlayers();
        this.showGameInfo();
        this.showRoundPlayer();
    }

    onResetBtnClick() {
        this.game.resetGame();
        this.clearView();
        this.startGame();
    }


    onNewGameBtnClick() {
        this.game.newGame();
        this.clearView();
        this.showGameInfo();
        this.showRoundPlayer();
    }

    initEvents() {
        this.squareBtns.forEach(button => {
            button.addEventListener('click', (event) => this.handleClick(event.target));
        });

        this.resetBtn.addEventListener('click', () => this.onResetBtnClick());

        this.newGameBtn.addEventListener('click', () => this.onNewGameBtnClick());
    }

    showRoundPlayer() {
        const player = this.game.isFirstPlayerTurn ? this.game.firstPlayer : this.game.secondPlayer;
        this.playerBlock.innerHTML = `<span class="player__name">${player.name}'s</span> turn:`;
    }

    showGameInfo() {
        this.infoBlock.innerHTML = `
            ${this.game.firstPlayer.name}: ${this.game.firstPlayer.mark} (Wins: ${this.game.firstPlayer._wins})
            ${this.game.secondPlayer.name}: ${this.game.secondPlayer.mark} (Wins: ${this.game.secondPlayer._wins})  
        `;
    }

    onGameEnd(message) {
        this.resultBlock.innerHTML = message;
    }

    clearView() {
        this.squareBtns.forEach(el => {
            el.innerHTML = '';
        });
        this.resultBlock.innerHTML = '';
    }
}