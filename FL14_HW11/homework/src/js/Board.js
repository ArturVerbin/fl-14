class Board {
    constructor(options) {
        this.squares = Array(options.squareCount).fill(null);
        this.changeBoard = options.callback;
    }

    onBoardChange() {
        this.changeBoard(this.squares);
    }
}

export default Board;