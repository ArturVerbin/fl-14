export default class Player {
    constructor(name) {
        this.name = name;
        this.mark = null;
        this._wins = 0;
    }

    set playerMark(mark) {
        this.mark = mark;
    }
}