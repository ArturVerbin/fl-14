import Player from './Player';

export default class TicTacToe {
    constructor(options) {
        this.squareButtons = document.getElementsByClassName('grid__item');
        this.board = options.board;
        this.onGameEnd = options.onGameEnd;
        this.isFirstPlayerTurn = true;
        this.winningCombinations = [
            [0,1,2],
            [3,4,5],
            [6,7,8],
            [0,3,6],
            [1,4,7],
            [2,5,8],
            [0,4,8],
            [2,4,6]
        ];
        this.isGameOver = false;
    }

    initPlayers() {
        this.players = [new Player('Artur'), new Player('Honey')];
        this.shufflePlayers();
    }

    setSquareValue(id, value) {
        const currentPlayer = this.getCurrentPlayer();
        if (this.board.squares[id] === null && !this.isGameOver) {
            this.board.squares[id] = value;
            this.board.onBoardChange();

            if(this.checkWinner(currentPlayer)) {
                currentPlayer._wins += 1;
                this.isGameOver = !this.isGameOver;
                this.endGame(false, currentPlayer);
            } else if (this.isDraw()) {
                this.endGame(true);
            } else {
                this.isFirstPlayerTurn = !this.isFirstPlayerTurn;
            }
        }
    }

    shufflePlayers() {
        const shuffledPlayers = this.players.sort(() => Math.random() - 0.5);
        this.firstPlayer = shuffledPlayers[0];
        this.secondPlayer = shuffledPlayers[1];
        this.firstPlayer.mark = 'X';
        this.secondPlayer.mark = 'O';
    }

    checkWinner(player) {
        return this.winningCombinations.some(combination => {
           return combination.every(index => {
                return this.board.squares[index] === player.mark;
           });
        });
    }

    endGame(draw, winner = undefined) {
        if(draw) {
            const message = `Draw!`;
            this.onGameEnd(message);
        } else {
            const message = `${winner.name}'s winner!`;
            this.onGameEnd(message);
        }
    }

    isDraw() {
        return [...this.squareButtons].every(element => {
            return element.innerHTML === 'X' || element.innerHTML === 'O';
        });
    }

    resetGame() {
        this.board.squares = this.board.squares.map(el => {
            el = null;
            return el;
        });
        this.players = [];
        this.firstPlayer = null;
        this.secondPlayer = null;
        this.isGameOver = false;
        this.isFirstPlayerTurn = true;
    }

    newGame() {
        this.board.squares = this.board.squares.map(el => {
            el = null;
            return el;
        });
        this.isGameOver = false;
        this.isFirstPlayerTurn = true;
        this.shufflePlayers();
    }

    getCurrentPlayer() {
        return this.isFirstPlayerTurn ? this.firstPlayer : this.secondPlayer;
    }
}
